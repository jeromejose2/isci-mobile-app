<?php
namespace CI\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CI\InventoryBundle\Entity\MobileApp;

class LoadMobileAppData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$mobileApp = new MobileApp();
		$mobileApp->setAppId('00000000000');
		$mobileApp->setAppSecret('00000000000');
		$mobileApp->setAppVersion('version 1.00');
		$mobileApp->setDataVersion('0');
		$mobileApp->setPin('00000000000');
		$mobileApp->setCreatedAt(new \DateTime());
		$mobileApp->setCreatedBy('default');
		
		$manager->persist($mobileApp);
		$manager->flush();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 2; // the order in which fixtures will be loaded
	}
}
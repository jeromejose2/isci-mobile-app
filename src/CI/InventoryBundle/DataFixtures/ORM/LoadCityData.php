<?php
namespace CI\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use CI\InventoryBundle\Entity\City;

class LoadCityData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$arrResult = array();
		$handle = fopen(realpath(dirname(__FILE__). '/../CSV/cities.csv'), 'r');
		if( $handle ) {
			while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
				$arrResult[] = $data;
			}
			fclose($handle);
		}
		
		echo "Handling Cities \n";
		foreach ($arrResult as $row => $col) {
			$entity = new City();
			$entity->setName($col[0]);
			$manager->persist($entity);
				
			echo "Processing ..... ";
			echo (integer)(($row / count($arrResult)) * 100) ."% \r";
		}
		
		echo "Processing ..... 100% \n";
		$manager->flush();
		echo "Done! \n";
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getOrder()
	{
		return 3; // the order in which fixtures will be loaded
	}
}
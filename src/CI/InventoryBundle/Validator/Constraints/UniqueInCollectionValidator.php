<?php

namespace CI\InventoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class UniqueInCollectionValidator extends ConstraintValidator
{
    private $collectionValues = array();

    public function validate($value, Constraint $constraint)
    {
        if($constraint->propertyPath) {
        	$propertyAccessor = new PropertyAccessor();
        	$value = $propertyAccessor->getValue($value, $constraint->propertyPath);
        }

        if(in_array($value, $this->collectionValues)) {
        	$this->context->addViolation($constraint->message, array());
        }

        $this->collectionValues[] = $value;
    }
}
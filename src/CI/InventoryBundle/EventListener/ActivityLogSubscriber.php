<?php
namespace CI\InventoryBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

use CI\InventoryBundle\Entity\ActivityLog;
use CI\CoreBundle\Entity\User;

class ActivityLogSubscriber implements EventSubscriber
{
    protected $serviceContainer;
    
    private $needsToFlush;
    
    private $entityIdToRemove;
    
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->needsToFlush = false;
    }
    
    public function getSubscribedEvents()
    {
         return array(
         	'postPersist',
         	'postUpdate',
         	'preRemove',
         	'postRemove',
         	'postFlush'
         );
    }
    
    public function postPersist(LifecycleEventArgs $args)
    {
    	$this->generateLog($args, ActivityLog::ACTION_CREATE);
    }
    
    public function postUpdate(LifecycleEventArgs $args)
    {
    	$this->generateLog($args, ActivityLog::ACTION_UPDATE);
    }
    
    public function preRemove(LifecycleEventArgs $args)
    {
    	// store the ID of the entity to be removed because doctrine will set the ID of the entity to NULL at postRemove 
    	$this->entityIdToRemove = $args->getEntity()->getId();
    }
    
    public function postRemove(LifecycleEventArgs $args)
    {
    	$this->generateLog($args, ActivityLog::ACTION_DELETE);
    }
    
    public function postFlush(PostFlushEventArgs $args)
    {
    	if ($this->needsToFlush) {
    		$this->needsToFlush = false;
    		$args->getEntityManager()->flush();
    	}
    }
    
    protected function generateLog(LifecycleEventArgs $args, $action)
    {
    	$em = $args->getEntityManager();
    	$entity = $args->getEntity();
    	$class = $this->getRealClass($entity);
    	
    	$excludedClasses = array(
    		'UserLog',
    		'User',
    		'LogEntry',
    		'ActivityLog'
    	);
    	
    	if (!in_array($class, $excludedClasses)) {
	    	$token = $this->serviceContainer->get('security.context')->getToken();
	    	if (is_object($token) && $token->getUser() instanceof User) {
	    		$userName = $token->getUser()->getName();
	    	} else {
	    		$userName = 'system';
	    	}
	    	
	        $routeName = strtolower($class) . '_show';
	        
        	$name = "";
        	if (method_exists($entity, 'getName')) {
        		$name = $entity->getName();
        	} elseif (method_exists($entity, '__toString')) {
        		$name = (string) $entity;
        	}

        	//get the class to be displayed on the activity log
        	$displayClass = method_exists($entity, 'getDisplayClass') ? $this->getDisplayClass(call_user_func(array($entity, 'getDisplayClass'))) : $this->getDisplayClass($class);
        	$log = new ActivityLog();
        	$log->setAction($action)
	        	->setUser($userName)
	        	->setTimestamp(new \DateTime('now'))
	        	->setClass($displayClass)
	        	->setName($name)
	        	->setRouteName($routeName)
	        	->setEntityId(isset($this->entityIdToRemove) ? $this->entityIdToRemove : $entity->getId());
        	;
        	
        	$em->persist($log);
        	$this->needsToFlush = true;
        }
    }
    
    public function getDisplayClass($class)
    {
    	return strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $class));
    }
    
    public function getRealClass($obj)
    {
    	$classname = is_object($obj) ? get_class($obj) : $obj;
    	 
    	$matches = array();
    
    	if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
    		$classname = $matches[1];
    	}
    
    	return $classname;
    }
}
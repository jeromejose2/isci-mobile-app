<?php

namespace CI\InventoryBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use CI\InventoryBundle\Helper\ApiResponse;

/**
 * @NamePrefix("api_")
 */
class ProductController extends FOSRestController
{

	 public function apilog($method = '', $name = '', $desc = '', $status = '')
    {

  date_default_timezone_set('Asia/Manila');
        // start setlog
        $sql_log = "INSERT INTO api_log (name,method,description,status,date_created) VALUES (:name,:method,:description,:status,:date_created); ";
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql_log);
        $log_arr['name'] = $name;
        $log_arr['method'] = $method;
        $log_arr['description'] = $desc;
        $log_arr['status'] = $status;
        $log_arr['date_created'] = date('Y-m-d H:i:s');
        $stmt->execute($log_arr);
        // end setlog

        return true;
    }

	/**
	 * @ApiDoc(
	 * 		description="Based from the device ID, it returns the list of products based from all the stores registered for the device ID in JSON format.",
     *  	statusCodes={
     *         200="Returned when successful.",
     *         403="Returned when the user is not authorized."
     *     }
	 * )
	 * 
	 */
	public function getProductlistAction()
	{
		//validate appID, appSecret, deviceID
		$aas = $this->get('ci_inventory.api.authentication.service');
		$device = $aas->isAuthenticated($this->getRequest());
		
		$apiResponse = $this->get('ci.api.response');
		
		if (empty($device)) {
			$apiResponse->setResponse(
				ApiResponse::ERROR,
				'Access not granted.123',
				array()
			);
			$view = $this->view($apiResponse, 403);
			return $this->handleView($view);
		}
		  $this->apilog('productlist-start', $device->getDeviceID(), '', 1);

		
		$products = $this->getDoctrine()->getRepository("CIInventoryBundle:Product")->getStoreProducts($device->getPersonInCharge());
		
		  $this->apilog('productlist-end', $device->getDeviceID(), json_encode( $products), 1);


		$apiResponse->setResponse(
			ApiResponse::SUCCESS,
			'',
			$products
		);
		
		$view = $this->view($apiResponse, 200);
		
		return $this->handleView($view);
	}
}
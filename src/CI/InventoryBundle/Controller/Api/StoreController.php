<?php

namespace CI\InventoryBundle\Controller\Api;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\FOSRestController;
use CI\InventoryBundle\Helper\ApiResponse;
use Doctrine\Common\Collections\Criteria;

/**
 * @NamePrefix("api_")
 */
class StoreController extends FOSRestController
{

		 public function apilog($method = '', $name = '', $desc = '', $status = '')
    {
  date_default_timezone_set('Asia/Manila');

        // start setlog
        $sql_log = "INSERT INTO api_log (name,method,description,status,date_created) VALUES (:name,:method,:description,:status,:date_created); ";
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql_log);
        $log_arr['name'] = $name;
        $log_arr['method'] = $method;
        $log_arr['description'] = $desc;
        $log_arr['status'] = $status;
        $log_arr['date_created'] = date('Y-m-d H:i:s');
        $stmt->execute($log_arr);
        // end setlog

        return true;
    }

	/**
	 * @ApiDoc(
	 * 		description="Gets the list off the stores assigned to the passed device id",
     *  	statusCodes={
     *         200="Returned when successful.",
     *         403="Returned when the user is not authorized."
     *     }
	 * )
	 */
	public function getStorelistAction()
	{
		//validate appID, appSecret, deviceID
		$aas = $this->get('ci_inventory.api.authentication.service');
		$device = $aas->isAuthenticated($this->getRequest());
		
		$apiResponse = $this->get('ci.api.response');
		 
		if (empty($device)) {
			$apiResponse->setResponse(
				ApiResponse::ERROR,
				'Access not granted.',
				array()
			);
			$view = $this->view($apiResponse, 403);
			return $this->handleView($view);
		}
		$this->apilog('storelist-start', $device->getDeviceID(), '' , 1);

		$id123 = $device->getPersonInCharge();
		$stores = $this->getDoctrine()->getRepository("CIInventoryBundle:Store")->findStoresByUser($device->getPersonInCharge());
//print_r('<pre>');
//print_r($stores);
//var_dump($id123);
//die();
		$data = array();
		$criteria = Criteria::create()->where(Criteria::expr()->eq('active', true));
		foreach($stores as $store) {
			$products = array();
				
			foreach($store->getProducts()->matching($criteria) as $product) {
				$products[] = $product->getId();
			}
				
			$data[] = array(
				'storeID' => $store->getId(),
				'storeName' => $store->getName(),
				'products' => $products
			);
		}
		
		  $this->apilog('storelist-end', $device->getDeviceID(), json_encode( $data), 1);

		$apiResponse->setResponse(
			ApiResponse::SUCCESS,
			'',
			$data
		);
		
		$view = $this->view($apiResponse, 200);
		
		return $this->handleView($view);
	}
}
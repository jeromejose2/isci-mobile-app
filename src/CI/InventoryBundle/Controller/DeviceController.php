<?php

namespace CI\InventoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

/**
 * Device controller.
 *
 * @Route("/device")
 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
 */
class DeviceController extends BaseController
{
	const LINK_SHOW = 'device_show';
	
	public function getModel()
	{
		return $this->get('ci.device.model');
	}
	
	/**
	 * Shows the Device Listing.
	 *
	 * @Route("/", name="device")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getFilterFormType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$qb = $model->getIndex($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (!isset($qb)) {
			$qb = $model->getIndex();
		}
		
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$qb,
			$this->get('request')->query->get('page', 1),
			$this->container->getParameter('pagination_limit_per_page'),
			array('distinct' => true)
		);
		
		return array(
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 * Creates a new Device entity.
	 *
	 * @Route("/", name="device_create")
	 * @Method("POST")
	 * @Template("CIInventoryBundle:Device:new.html.twig")
	 */
	public function createAction(Request $request)
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->saveEntity($form, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages('create'));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Displays a form to create a new Device entity.
	 *
	 * @Route("/new", name="device_new")
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction()
	{
		$model = $this->getModel();
		$entity = $model->getNewEntity();
		$form = $model->getFormType($entity);
		
		return array(
			'entity' => $entity,
			'form'   => $form->createView()
		);
	}
	
	/**
	 * Finds and displays a Device entity.
	 *
	 * @Route("/{id}/show", name="device_show")
	 * @Method("GET")
	 * @Template()
	 */
	public function showAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		return array(
			'entity' => $entity
		);
	}
	
	/**
	 * Displays a form to edit an existing Device entity.
	 *
	 * @Route("/{id}/edit", name="device_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$editForm = $model->getFormType($entity);
		
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		);
	}
	
	/**
	 * Edits an existing Device entity.
	 *
	 * @Route("/{id}", name="device_update")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Device:edit.html.twig")
	 */
	public function updateAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		
		$editForm = $model->getFormType($entity);
		
		if ($editForm->handleRequest($request)->isSubmitted()) {
			if ($editForm->isValid()) {
				try {
					$model->saveEntity($editForm, $entity);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages('update'));
					return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
			}
		}
	
		return array(
			'entity'    => $entity,
			'edit_form' => $editForm->createView()
		);
	}
	
	/**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", name="device_confirm_delete")
	 * @Method("GET")
	 * @Template("CIInventoryBundle:Misc:delete.html.twig")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}
	
	/**
	 * Deletes a Device entity.
	 *
	 * @Route("/{id}", name="device_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$form = $this->createDeleteForm($id);
		
		try {
			$model->isDeletable($entity);
		} catch (\Exception $e) {
			$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
			return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
		}
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages('delete'));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}
		
		return $this->redirect($this->generateUrl('device'));
	}
	
	/**
	 * Confirm status change of a Device entity.
	 *
	 * @Route("/{id}/confirm-change-status/{status}", name="device_confirm_change_status")
	 * @Template("CIInventoryBundle:Device:changeStatus.html.twig")
	 */
	public function confirmChangeStatusAction($id, $status)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
	
		return $model->confirmChangeStatus($entity, $status, 'device_status_change', self::LINK_SHOW);
	}
	
	/**
	 * Change status of a Device entity.
	 *
	 * @Route("/{id}/status-change", name="device_status_change")
	 * @Method("PUT")
	 * @Template("CIInventoryBundle:Device:changeStatus.html.twig")
	 */
	public function changeStatusAction($id, Request $request)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$changeStatusForm = $model->createChangeStatusForm();
	
		if ($changeStatusForm->handleRequest($request)->isSubmitted()) {
			if ($changeStatusForm->isValid()) {
				$newStatus = $changeStatusForm->get('status')->getData();
				try {
					$status = $model->changeStatus($newStatus, $entity);
					$this->get('session')->getFlashBag()->add('success', sprintf('This device has been set to %s.', $status));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Sorry, unable to change the status. Please try again.');
			}
		}
		 
		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
	}
	
	/**
	 * @Route("/revision/{id}", name="device_revision")
	 * @Template("CIInventoryBundle:Log:index.html.twig")
	 */
	public function revisionAction($id)
	{
		$model = $this->getModel();
		$revision = $model->getRevision($id);
		return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
	}
	
	/**
	 * @Route("/revision/{parentId}/{id}/log", name="device_log")
	 * @Template("CIInventoryBundle:Log:show.html.twig")
	 */
	public function logAction($parentId, $id)
	{
		$model = $this->getModel();
		$log = $model->getLog();
		return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
	}
}
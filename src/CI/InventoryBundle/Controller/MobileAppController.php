<?php
namespace CI\InventoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;

use CI\InventoryBundle\Model\MobileAppModel as Model;

/**
 * MobileApp controller
 *
 * @Route("/mobile-app-settings")
 * @PreAuthorize("hasRole('ROLE_IT_ACCOUNT')")
 */
class MobileAppController extends BaseController
{
	public function getModel()
	{
		return $this->get('ci.mobileapp.model');
	}
	
    /**
     * MobileApp
     *
     * @Route("/", name="mobileapp_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Request $request)
    {
    	if (!is_null($request->get('id'))) {
    		return $this->redirect($this->generateUrl('mobileapp_show'));
    	}
    	
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity();
    	
    	return array(
    		'entity' => $entity
    	);
    }
    
    /**
     * Displays a form to edit an existing MobileApp entity.
     *
     * @Route("/edit", name="mobileapp_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity();
    	 
    	$editForm = $model->getFormType($entity);
    	 
    	return array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView()
    	);
    }
    
    /**
     * Edits an existing MobileApp entity.
     *
     * @Route("/", name="mobileapp_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity();
    	 
    	$editForm = $model->getFormType($entity);
    	 
    	if ($editForm->handleRequest($request)->isSubmitted()) {
    		if ($editForm->isValid()) {
    			try {
    				$model->saveEntity($editForm, $entity);
    				$this->get('session')->getFlashBag()->add('success', 'Successfully updated the mobile app settings.');
    				return $this->redirect($this->generateUrl('mobileapp_show'));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
    		}
    	}
    	 
    	return $this->render('CIInventoryBundle:MobileApp:edit.html.twig', array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView()
    	));
    }
    
    /**
     * @Route("/revision/{id}", requirements={"id"="\d+"}, name="mobileapp_revision")
     * @Template("CIInventoryBundle:Log:index.html.twig")
     */
    public function revisionAction($id)
    {
    	$model = $this->getModel();
    	$revision = $model->getRevision($id);
    	return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
    }
    
    /**
     * @Route("/revision/{parentId}/{id}/log", requirements={"id"="\d+", "parentId"="\d+"}, name="mobileapp_log")
     * @Template("CIInventoryBundle:Log:show.html.twig")
     */
    public function logAction($parentId, $id)
    {
    	$model = $this->getModel();
    	$log = $model->getLog();
    	return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
    }
}
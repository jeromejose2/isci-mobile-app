<?php
namespace CI\InventoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use PHPExcel;
use PHPExcel_IOFactory;

/**
 * Store Report controller
 *
 * @Route("/report/store")
 */
class StoreReportController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.store_report.model');
	}
	
	/**
	 * SummaryOSAReport
	 *
	 * @Route("/summary-osa", name="storereport_summaryosa")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
	 */
	public function summaryOSAReportAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getSummaryOSAReportFilterType();
	
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$data = $model->getSummaryOSAReportData($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
	
		return array(
			'params' => isset($params) ? $params : null,
			'data' => isset($data) ? $data : null,
			'search_form' => $form->createView()
		);
	}
	
	/**
	 * @Route("/summary-osa/export/xls", name="storereport_summaryosa_export_xls")
	 * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
	 */
	public function summaryOSAReportXlsAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getSummaryOSAReportFilterType();
		$form->handleRequest($request);
		$params = $form->getData();
              
		$data = $model->exportSummaryOSAReport($params);
		
		$response = new Response();
		$response->headers->set('Content-Type', 'application/vnd.ms-excel');
		$response->headers->set('Content-Disposition', 'attachment;filename='. $data['filename'] . '.xls');
		$response->headers->set('Cache-Control', 'ax-age=0');
		$response->sendHeaders();
	
		$data['objWriter']->save('php://output');
		exit();
	}
	
	/**
	 * BranchOSAReport
	 *
	 * @Route("/branch-osa", name="storereport_branchosa")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
	 */
	public function branchOSAReportAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getBranchOSAReportFilterType();
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$data = $model->getBranchOSAReportData($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		return array(
			'params' => isset($params) ? $params : null,
			'data' => isset($data) ? $data : null,
			'search_form' => $form->createView()
		);
	}
	
	/**
	 * @Route("/branch-osa/export/xls", name="storereport_branchosa_export_xls")
	 * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
	 */
	public function branchOSAReportXlsAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getBranchOSAReportFilterType();
		$form->handleRequest($request);
		$params = $form->getData();
		$data = $model->exportBranchOSAReport($params);
		
		$response = new Response();
		$response->headers->set('Content-Type', 'application/vnd.ms-excel');
		$response->headers->set('Content-Disposition', 'attachment;filename='. $data['filename'] . '.xls');
		$response->headers->set('Cache-Control', 'ax-age=0');
		$response->sendHeaders();
		
		$data['objWriter']->save('php://output');
		exit();
	}
	
	/**
	 * @Route("/download-osa", name="get_download_osa")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
	 */
	public function getDownloadOsaAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getDownloadOsa('filter');
	
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'An error occurred. Please try again.');
			}
		}
	
		return array(
			'params' => isset($params) ? $params : null,
			'search_form' => $form->createView(),
		);
	}
	
	/**
	 * @Route("/download-osa/export/xls", name="get_download_osa_export_xls")
	 * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
	 */
	public function getDownloadOsaExportXlsAction(Request $request)
	{
		$model = $this->getModel();
		$form = $model->getDownloadOsa('filter');
		$form->handleRequest($request);
		$params = $form->getData();
		$data = $model->getDownloadOsa('xls', $params);
		 
		$response = new Response();
		$response->headers->set('Content-Type', 'application/vnd.ms-excel');
		$response->headers->set('Content-Disposition', 'attachment;filename='. $data['filename'] . '.xls');
		$response->headers->set('Cache-Control', 'ax-age=0');
		$response->sendHeaders();
	
		$data['objWriter']->save('php://output');
		exit();
	}
}
<?php
namespace CI\InventoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\PreAuthorize;
use PHPExcel;
use PHPExcel_IOFactory;

use CI\CoreBundle\Entity\User;
use CI\InventoryBundle\Form\Type\UmReportFilterType;

/**
 * UmReport controller
 *
 * @Route("/umreport")
 */
class UmReportController extends Controller
{
    	/**
	 * InventoryReport
	 *
	 * @Route("/", name="um-report")
	 * @Method("GET")
	 * @Template()
	 * @PreAuthorize("hasAnyRole('ROLE_RSM', 'ROLE_CDM', 'ROLE_TL')")
	 */
    public function indexAction(Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		$form = $this->createForm(new UmReportFilterType($this->get('security.context')));
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				$params = $form->getData();
				$params = $this->prepareParams($params);
				
				$qb = $em->getRepository('CIInventoryBundle:UmReport')->findAllUMReport($params);
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
			}
		}
		
		if (isset($qb)) {
			$paginator = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
				$qb,
				$this->get('request')->query->get('page', 1),
				$this->container->getParameter('pagination_limit_per_page'),
				array('distinct' => true)
			);
		}
		
		return array(
			'params' => isset($params) ? $params : null,
			'pagination' => isset($pagination) ? $pagination : null,
			'search_form' => $form->createView(),
		);
    }
    
    private function prepareParams(array $params)
	{
		$sc = $this->get('security.context');
		
		if (!$sc->isGranted(User::ROLE_ADMIN)) {
			if ($sc->isGranted(User::ROLE_RSM)) {
				$params['rsm'] = $this->getUser();
			} else if ($sc->isGranted(User::ROLE_CDM)) {
				$params['cdm'] = $this->getUser();
			} else if ($sc->isGranted(User::ROLE_TL)) {
				$params['tl'] = $this->getUser();
			} else if ($sc->isGranted(User::ROLE_DISER)) {
				$params['diser'] = $this->getUser();
			}
		}
		
		return $params;
	}
        
     
}

<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use PHPExcel;
use PHPExcel_IOFactory;

use CI\CoreBundle\Entity\User;
//use CI\InventoryBundle\Form\Type\SummaryOSAReportFilterType;
//use CI\InventoryBundle\Form\Type\BranchOSAReportFilterType;
//use CI\InventoryBundle\Form\Type\DownloadOsaFilterType;
//use CI\InventoryBundle\Form\Type\InventoryReportFilterType;

use CI\InventoryBundle\Form\Type\UmReportFilterType;

class UmReportModel extends BaseEmptyEntityModel
{
	public function getUmReportFilterType()
	{
            return $this->getFormFactory()->create(new UmReportFilterType($this->getSecurityContext()));
	}


}
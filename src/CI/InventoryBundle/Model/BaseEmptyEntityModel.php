<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\InventoryBundle\Entity\BaseEntity;

abstract class BaseEmptyEntityModel
{
	private $container;
	private $repositoryName;
	
	public function __construct(ContainerInterface $container, $repositoryName = null)
	{
		$this->container = $container;
		
		if (is_string($repositoryName) || $repositoryName === null) {
			$this->repositoryName = $repositoryName;
		} else {
			throw new \Exception("Invalid repository name.");
		}
	}
	
	public function getContainer()
	{
		return $this->container;
	}
	
	public function getRouter()
	{
		return $this->container->get('router');
	}
	
	public function getFormFactory()
	{
		return $this->container->get('form.factory');
	}
	
	public function getTemplateEngine()
	{
		return $this->container->get('templating');
	}
	
	public function getSecurityContext()
	{
		return $this->container->get('security.context');
	}
	
	public function getEM()
	{
		return $this->container->get('doctrine')->getManager();
	}
	
	public function getSession()
	{
		return $this->container->get('session');
	}
	
	public function getRepository($repositoryName = null)
	{
		$repositoryName = $repositoryName === null ? $this->repositoryName : $repositoryName;
		return $this->getEM()->getRepository($repositoryName);
	}
	
	/**
	 * Returns true if the service id is defined.
	 *
	 * @param string $id The service id
	 *
	 * @return bool  true if the service id is defined, false otherwise
	 */
	public function has($id)
	{
		return $this->container->has($id);
	}
	
	/**
	 * Gets a service by id.
	 *
	 * @param string $id The service id
	 *
	 * @return object The service
	 */
	public function get($id)
	{
		return $this->container->get($id);
	}
}
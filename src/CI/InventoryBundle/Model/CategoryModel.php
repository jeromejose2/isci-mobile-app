<?php

namespace CI\InventoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

use CI\InventoryBundle\Entity\Category as EntityType;
use CI\InventoryBundle\Form\Type\CategoryType as EntityFormType;
use CI\InventoryBundle\Form\Type\SearchFilterType as FilterFormType;

class CategoryModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';
	
	public function getNewEntity()
	{
		return new EntityType();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new EntityFormType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType('category'), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: return 'Category created successfully.';
			case self::ACTION_UPDATE: return 'Category updated successfully.';
			case self::ACTION_DELETE: return 'Category has been deleted.';
			case self::ACTION_STATUS_CHANGE : return 'Category status has been updated to ';
			default: throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(EntityType $entity)
	{
 		if (!$entity->isDeletable()) {
 			throw new \Exception('This category has already been added to a product therefore can no longer be deleted.');
 		}
	}
	
	public function isEditable(EntityType $entity)
	{
	}
	
	public function getIndex($params = null)
	{
		return $this->getRepository()->findAll($params);
	}
	
	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		
		if ($form->get('save')->isClicked()) {
			
		} else {
			throw new \Exception("Invalid operation. Please try again.");
		}
		
		$em->persist($entity);
		
		$settings = $em->getRepository('CIInventoryBundle:MobileApp')->find(1);
		$settings->setDataVersion($settings->getDataVersion() + 1);
		$em->persist($settings);
		$em->flush();
	}
	
	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'category_delete',
    		'return_path' => 'category_show',
    		'name' => '[Category] ' . $entity->getName()
    	);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Category'
		);
	
		$options = array(
			'route' => 'category',
			'name' => 'Category',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'category',
			'name' => 'Category',
			'classes' => array(
				'CI\InventoryBundle\Entity\Category'
			)
		);
	}
}
<?php

namespace CI\InventoryBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;

class DashboardModel
{
	private $container;
	private $em;
	private $securityContext;
	
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
		$this->securityContext = $container->get('security.context');
		$this->em = $container->get('doctrine')->getManager();
	}
	
	public function getIndex()
	{
		return array();
	}
}
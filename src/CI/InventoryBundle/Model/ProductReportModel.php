<?php

namespace CI\InventoryBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface;
use PHPExcel;
use PHPExcel_IOFactory;

use CI\CoreBundle\Entity\User;
use CI\InventoryBundle\Form\Type\CoreAverageReportFilterType;
use CI\InventoryBundle\Form\Type\FrequencyVsOkReportFilterType;

class ProductReportModel
{
	private $container;
	private $formFactory;
	private $em;
	private $sc;
	
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
		$this->formFactory = $container->get('form.factory');
		$this->em = $container->get('doctrine')->getManager();
		$this->sc = $container->get('security.context');
	}
	
	private function prepareParams(array &$params)
	{
		$sc = $this->sc;
		$user = $sc->getToken()->getUser();
		if (!$sc->isGranted(User::ROLE_ADMIN)) {
			if ($sc->isGranted(User::ROLE_RSM)) {
				$params['rsm'] = $user;
			} else if ($sc->isGranted(User::ROLE_CDM)) {
				$params['cdm'] = $user;
			} else if ($sc->isGranted(User::ROLE_TL)) {
				$params['tl'] = $user;
			} else if ($sc->isGranted(User::ROLE_DISER)) {
				$params['diser'] = $user;
			}
		}
	}
	
	public function getCoreAverageReport($type, array $params = null, $isDownload = false, $xls = null)
	{
		switch ($type) {
			case 'filter':
				return $this->formFactory->create(new CoreAverageReportFilterType($this->sc));
			case 'index':
				return $this->getCoreAverageReport('data', $params);
			case 'data':
				$em = $this->em;
				$this->prepareParams($params);    
                                  
				$percentage = $em->getRepository('CIInventoryBundle:InventoryReport')->getCoreAverage($params); 
                                
				$totals = $em->getRepository('CIInventoryBundle:InventoryReport')->getCoreAverageTotals($params);
				
				$results = $regions = $products = array();
				
				foreach ($percentage as $result) {
					if (!array_key_exists($result['regionId'], $regions)) {
						$regions[$result['regionId']] = $result['regionName'];
					}
					if (!array_key_exists($result['productId'], $products)) {
						$products[$result['productId']] = $result['productName'];
					}
					$results[$result['regionId']][$result['productId']] = $result['percentage'];
				}
				
				return array(
					'data' => $results, 
					'totals' => array_column($totals, null, 'productId'),
					'products' => $products, 
					'regions' => $regions,
					'dataCount' => count($percentage)
				);
			case 'xls':
				$letterLimit = 'A';
				$boldStyle = array('bold' => true);
				$rightStyle = array('alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
				$centerStyle = array('font' => $boldStyle,'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
				$borderThin = array('style' => \PHPExcel_Style_Border::BORDER_THIN);
				$bordersStyle = array('borders' => array('allborders' => $borderThin));
				$bodyBorderStyle = array('borders' => array('left' => $borderThin, 'right' => $borderThin));
				
				if (!$isDownload) {
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->getProperties()->setCreator('ISCI')->setTitle('Core Average Report');
					$xls = $objPHPExcel->setActiveSheetIndex(0);
					$objPHPExcel->getActiveSheet()->setTitle('Core Average');
				}
				
				$data = $this->getCoreAverageReport('data', $params);
				
				if ($data['dataCount'] > 0) {
					//headers
					$xls->setCellValue('A1', 'Region');
					$xls->getColumnDimension('A')->setWidth(20);
					$xls->getStyle('A1')->applyFromArray(array('font' => $boldStyle));
					$letterStart = 'B';
					foreach ($data['products'] as $productName) {
						$xls->setCellValue($letterStart . '1', $productName);
						$xls->getColumnDimension($letterStart)->setWidth(35);
						$letterLimit = $letterStart;
						$letterStart++;
					}
					$xls->getStyle('B1:' . $letterLimit . '1')->applyFromArray($centerStyle);
					$xls->getStyle('A1:' . $letterLimit . '1')->applyFromArray($bordersStyle);
					
					//table data
					$counter = 2;
					foreach ($data['regions'] as $regionId => $regionName) {
						$xls->setCellValue('A' . $counter, $regionName);
						
						$letterStart = 'B';
						foreach ($data['products'] as $productId => $productName) {
							$xls->setCellValue($letterStart . $counter, (isset($data['data'][$regionId][$productId]) ? $data['data'][$regionId][$productId] : "0") . '%');
							$xls->getStyle($letterStart . $counter)->applyFromArray($bodyBorderStyle);
							$letterLimit = $letterStart;
							$letterStart++;
						}
							
						$counter++;
					}
					$xls->getStyle('B2:' . $letterLimit . $counter)->applyFromArray($rightStyle);
					
					//footer totals
					$xls->setCellValue('A' . $counter, 'Total Result');
					$xls->getStyle('A' . $counter)->applyFromArray(array('font' => $boldStyle));
					$letterStart = 'B';
					foreach ($data['products'] as $productId => $productName) {
						$percentage = 0;
						if (isset($data['totals'][$productId])) {
							$percentage = $data['totals'][$productId]['percentage'];
						}
						$xls->setCellValue($letterStart . $counter, $percentage . '%');
						$letterLimit = $letterStart;
						$letterStart++;
					}
					$xls->getStyle('B' . $counter . ':' . $letterStart . $counter)
						->applyFromArray(array('font' => $boldStyle, $rightStyle));
					$xls->getStyle('A' . $counter . ':' . $letterLimit . $counter)->applyFromArray($bordersStyle);
				}
				
				$xls->setSelectedCell();
				
				if (!$isDownload) {
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					$filename = 'Core_Average_Report_' . date('M-d-Y');
					return array('objWriter' => $objWriter, 'filename' => $filename);
				} else {
					return $xls;
				}
		}
	}
	
	public function getFrequencyVsOkReport($type, array $params = null, $isDownload = false, $xls = null)
	{
		switch ($type) {
			case 'filter':
				return $this->formFactory->create(new FrequencyVsOkReportFilterType($this->sc));
			case 'index':
				$this->prepareParams($params);
				return $this->em->getRepository('CIInventoryBundle:InventoryReport')->getFrequencyVsOk($params);
			case 'xls':
				$boldStyle = array('bold' => true);
				$rightStyle = array('alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT));
				$centerStyle = array('font' => $boldStyle,'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
				$bordersStyle = array('borders' => array('allborders' => array('style' => \PHPExcel_Style_Border::BORDER_THIN)));
	
				if (!$isDownload) {
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->getProperties()->setCreator('ISCI')->setTitle('Total Chain Average Report');
					$xls = $objPHPExcel->setActiveSheetIndex(0);
					$objPHPExcel->getActiveSheet()->setTitle('Total Chain Average Report');
				}
	
				$data = $this->getFrequencyVsOkReport('index', $params);
	
				if (count($data) > 0) {
					//headers
					$xls->setCellValue('A1', 'Category');
					$xls->setCellValue('B1', 'Product');
					$xls->setCellValue('C1', 'Total');
					$xls->setCellValue('D1', 'Ok');
					$xls->setCellValue('E1', '%');
					$xls->getColumnDimension('A')->setWidth(15);
					$xls->getColumnDimension('B')->setWidth(40);
					$xls->getColumnDimension('C')->setWidth(10);
					$xls->getColumnDimension('D')->setWidth(10);
					$xls->getColumnDimension('E')->setWidth(10);
					$xls->getStyle('A1:E1')->applyFromArray($centerStyle)
						->applyFromArray(array('fill' => array(
								'type' => \PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'CAE1FF'))));
					
					//data
					$counter = 2;
					foreach ($data as $result) {
						$xls->setCellValue('A' . $counter, $result['categoryName']);
						$xls->setCellValue('B' . $counter, $result['productName']);
						$xls->setCellValue('C' . $counter, $result['totalVisit']);
						$xls->setCellValue('D' . $counter, $result['totalAvailable']);
						$xls->setCellValue('E' . $counter, $result['percentage'] . '%');
						
						//set row color
						$color = '';
						if ($result['isCore']) {
							$color = 'BFEFFF';
						} elseif ($result['isNpd']) {
							$color = 'FFCCCC';
						} elseif ($result['isSeasonal']) {
							$color = 'FAFAD2';
						}
						
						if ($color) {
							$xls->getStyle('A' . $counter . ':B' . $counter)->applyFromArray(
									array('fill' => array(
										'type' => \PHPExcel_Style_Fill::FILL_SOLID,
										'color' => array('rgb' => $color)
									)
								)
							);
						}
						
						$counter++;
					}
					
					$xls->getStyle('C2:E' . $counter)->applyFromArray($rightStyle)->getNumberFormat()->setFormatCode('#,##0');
					$xls->getStyle('A1:E' . ($counter - 1))->applyFromArray($bordersStyle);
				}
	
				$xls->setSelectedCell();
	
				if (!$isDownload) {
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					$filename = 'Total_Chain_Average_Report_' . date('M-d-Y');
					return array('objWriter' => $objWriter, 'filename' => $filename);
				} else {
					return $xls;
				}
				
		}
	}
}
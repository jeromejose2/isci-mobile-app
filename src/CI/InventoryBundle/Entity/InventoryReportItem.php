<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use CI\InventoryBundle\Helper\ApiUploadedFile;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * InventoryReportItem
 *
 * @ORM\Table(name="inventory_report_item")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"validate"})
 */
class InventoryReportItem
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="available", type="boolean")
	 * @Assert\Choice(choices={1, 0}, message="Available should be 1 or 0.")
	 * @Assert\NotBlank(message="Available is required.")
	 */
	private $available;
	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="count", type="decimal", scale=2, precision=9, nullable=true)
	 * @Assert\Range(min=0, max=9999999.99, invalidMessage="Count should be between 0 and 9999999.99.")
	 * @Assert\Type(type="numeric", message="Count should be a valid number.")
	 */
	private $count;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="note", type="text", nullable=true)
	 * @Assert\Type(type="string")
	 */
	private $note;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=255)
	 */
	private	$path;
	
	/**
	 * @Assert\File(maxSize="150M")
	 */
	private $file;
	
	private $temp;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="inventoryReportItems")
	 * @Assert\NotBlank(message="Product is required.")
	 */
	private $product;
	
	/**
	 * @ORM\ManyToOne(targetEntity="InventoryReport", inversedBy="items")
	 */
	private $inventoryReport;
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set available
	 *
	 * @param boolean $available
	 * @return InventoryReportItem
	 */
	public function setAvailable($available)
	{
		$this->available = $available;
	
		return $this;
	}
	
	/**
	 * Get available
	 *
	 * @return boolean
	 */
	public function getAvailable()
	{
		return $this->available;
	}
	
	/**
	 * Set count
	 *
	 * @param string $count
	 * @return InventoryReportItem
	 */
	public function setCount($count)
	{
		$this->count = $count;
	
		return $this;
	}
	
	/**
	 * Get count
	 *
	 * @return string
	 */
	public function getCount()
	{
		return $this->count;
	}
	
	/**
	 * Set note
	 *
	 * @param string $note
	 * @return InventoryReportItem
	 */
	public function setNote($note)
	{
		$this->note = $note;
	
		return $this;
	}
	
	/**
	 * Get note
	 *
	 * @return string
	 */
	public function getNote()
	{
		return $this->note;
	}
	
	/**
	 * Set product
	 *
	 * @param \CI\InventoryBundle\Entity\Product $product
	 * @return InventoryReportItem
	 */
	public function setProduct(\CI\InventoryBundle\Entity\Product $product = null)
	{
		$this->product = $product;
	
		return $this;
	}
	
	/**
	 * Get product
	 *
	 * @return \CI\InventoryBundle\Entity\Product
	 */
	public function getProduct()
	{
		return $this->product;
	}

    /**
     * Set path
     *
     * @param string $path
     * @return InventoryReportItem
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Set inventoryReport
     *
     * @param \CI\InventoryBundle\Entity\InventoryReport $inventoryReport
     * @return InventoryReportItem
     */
    public function setInventoryReport(\CI\InventoryBundle\Entity\InventoryReport $inventoryReport = null)
    {
    	$this->inventoryReport = $inventoryReport;
    
    	return $this;
    }
    
    /**
     * Get inventoryReport
     *
     * @return \CI\InventoryBundle\Entity\InventoryReport
     */
    public function getInventoryReport()
    {
    	return $this->inventoryReport;
    }
    
    /**
     * Sets file.
     *
     * @param ApiUploadedFile $file
     */
    public function setFile(ApiUploadedFile $file = null)
    {
    	$this->file = $file;
    	// check if we have an old image path
    	if (is_file($this->getAbsolutePath())) {
    		// store the old name to delete after the update
    		$this->temp = $this->getAbsolutePath();
    	} else {
    		$this->path = 'initial';
    	}
    }
    
    /**
     * Get file.
     *
     * @return ApiUploadedFile
     */
    public function getFile()
    {
    	return $this->file;
    }
    
    public function getAbsolutePath()
    {
    	return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }
    
    public function getWebPath()
    {
    	return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
    }
    
    protected function getUploadRootDir()
    {
    	return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
    	return 'uploads/inventory-report/' . $this->getInventoryReport()->getId();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
    	if (null !== $this->getFile()) {
    		// do whatever you want to generate a unique name
    		$filename = sha1(uniqid(mt_rand(), true));
    		$this->path = $filename . '.' . $this->getFile()->guessExtension();
    	}
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null === $this->getFile()) {
    		return;
    	}
    	
    	$this->getFile()->move($this->getUploadRootDir(), $this->path);
    	
    	// check if we have an old image
    	if (isset($this->temp)) {
    		// delete the old image
    		unlink($this->getUploadRootDir().'/'.$this->temp);
    		// clear the temp image path
    		$this->temp = null;
    	}
    	$this->file = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	$file = $this->getAbsolutePath();
    	if ($file) {
    		unlink($file);
    	}
    }
    
    public function validate(ExecutionContextInterface $context)
    {
    	if ($this->file) {
    		if (!file_exists($this->file->getPathname())) {
    			$context->addViolationAt('file', 'Image not found.');
    		}
    		
    		if (!in_array($this->file->guessExtension(), array('jpg', 'png', 'jpeg'))) {
    			$context->addViolationAt('file', 'Only .jpg, .png and .jpeg images are allowed.');
    		}
    	}
    }
    
    public function getLog()
    {
    	return array(
    		'Product' => $this->getProduct()->getName(),
    		'Available' => $this->getAvailable() ? 'Yes' : 'No',
    		'Count' => number_format($this->getCount(), 2),
    		'Note' => $this->getNote()
    	);
    }
}
<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\CategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="name", message="Category already exists.")
 */
class Category extends BaseEntity
{
	/**
	 * @ORM\Column(name="name", type="string", length=255, unique=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter a category name.")
	 */
	protected $name;
	
	/**
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="category", cascade={"persist"})
	 */
	protected $products;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->products = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Category
	 */
	public function setName($name)
	{
		$this->name = $name;
	
		return $this;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Add products
	 *
	 * @param \CI\InventoryBundle\Entity\Product $products
	 * @return Category
	 */
	public function addProduct(\CI\InventoryBundle\Entity\Product $products)
	{
		$this->products[] = $products;
	
		return $this;
	}
	
	/**
	 * Remove products
	 *
	 * @param \CI\InventoryBundle\Entity\Product $products
	 */
	public function removeProduct(\CI\InventoryBundle\Entity\Product $products)
	{
		$this->products->removeElement($products);
	}
	
	/**
	 * Get products
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProducts()
	{
		return $this->products;
	}
	
	public function getLog()
	{
		return array(
			'Name' => $this->getName()
		);
	}
	
	public function isEditable()
	{
// 		$result = false;
		
// 		if ($this->getStatus() < self::STATUS_VOID) {
			$result = true;
// 		}
		
		return $result;
	}
	
	public function isDeletable()
	{
		if ($this->getProducts()->count() > 0) {
			return false;
		}
		
		return true;
	}
}
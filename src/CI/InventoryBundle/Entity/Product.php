<?php

namespace CI\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="CI\InventoryBundle\Entity\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="name", message="This name is already in use.")
 * @UniqueEntity(fields="refCode", message="This code is already in use.")
 */
class Product extends BaseEntity
{
	const CORE = 1;
	const NPD = 2;
	const SEASONAL = 3;
	const OSA_TRACKED = 4;
	
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_core", type="boolean")
	 */
	private $isCore = false;
	
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_npd", type="boolean")
	 */
	private $isNpd = false;
	
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_osa_tracked", type="boolean")
	 */
	private $isOsaTracked = false;
	
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_seasonal", type="boolean")
	 */
	private $isSeasonal = false;
	
	/**
	 * @ORM\Column(name="name", type="string", length=255, unique=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter a product name.")
	 */
	protected $name;
	
	/**
	 * @ORM\Column(name="description", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 */
	protected $description;
	
	/**
	 * @ORM\Column(name="ref_code", type="string", length=255, unique=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter a product reference code.")
	 */
	protected $refCode;
	
	/**
	 * @ORM\Column(name="size", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter a product size.")
	 */
	protected $size;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
	 * @Assert\NotBlank(message="Please enter the category of this product.")
	 */
	protected $category;
	
	/**
	 * @ORM\ManyToMany(targetEntity="Store", inversedBy="products")
	 * @ORM\JoinTable(name="products_stores")
	 * @ORM\OrderBy({"name"="ASC"})
	 * @Assert\Valid
	 */
	protected $stores;
	
	/**
	 * @ORM\OneToMany(targetEntity="InventoryReportItem", mappedBy="product")
	 */
	private $inventoryReportItems;
	/**
	 * @ORM\OneToMany(targetEntity="UmReportItem", mappedBy="product")
	 */
	private $umReportItems;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->stores = new ArrayCollection();
		$this->inventoryReportItems = new ArrayCollection();
		$this->umReportItems = new ArrayCollection();
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Product
	 */
	public function setName($name)
	{
		$this->name = $name;
	
		return $this;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Set refCode
	 *
	 * @param string $refCode
	 * @return Product
	 */
	public function setRefCode($refCode)
	{
		$this->refCode = $refCode;
	
		return $this;
	}
	
	/**
	 * Get refCode
	 *
	 * @return string
	 */
	public function getRefCode()
	{
		return $this->refCode;
	}
	
	/**
	 * Set size
	 *
	 * @param string $size
	 * @return Product
	 */
	public function setSize($size)
	{
		$this->size = $size;
	
		return $this;
	}
	
	/**
	 * Get size
	 *
	 * @return string
	 */
	public function getSize()
	{
		return $this->size;
	}
	
	/**
	 * Set category
	 *
	 * @param \CI\InventoryBundle\Entity\Category $category
	 * @return Product
	 */
	public function setCategory(\CI\InventoryBundle\Entity\Category $category = null)
	{
		$this->category = $category;
	
		return $this;
	}
	
	/**
	 * Get category
	 *
	 * @return \CI\InventoryBundle\Entity\Category
	 */
	public function getCategory()
	{
		return $this->category;
	}
	
	/**
	 * Add stores
	 *
	 * @param \CI\InventoryBundle\Entity\Store $stores
	 * @return Product
	 */
	public function addStore(\CI\InventoryBundle\Entity\Store $stores)
	{
		$stores->addProduct($this);
		$this->stores[] = $stores;
	
		return $this;
	}
	
	/**
	 * Remove stores
	 *
	 * @param \CI\InventoryBundle\Entity\Store $stores
	 */
	public function removeStore(\CI\InventoryBundle\Entity\Store $stores)
	{
		$this->stores->removeElement($stores);
	}
	
	/**
	 * Get stores
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getStores()
	{
		return $this->stores;
	}
	
	/**
	 * Add inventoryReportItems
	 *
	 * @param \CI\InventoryBundle\Entity\InventoryReportItem $inventoryReportItems
	 * @return Product
	 */
	public function addInventoryReportItem(\CI\InventoryBundle\Entity\InventoryReportItem $inventoryReportItems)
	{
		$this->inventoryReportItems[] = $inventoryReportItems;
	
		return $this;
	}
	
	/**
	 * Remove inventoryReportItems
	 *
	 * @param \CI\InventoryBundle\Entity\InventoryReportItem $inventoryReportItems
	 */
	public function removeInventoryReportItem(\CI\InventoryBundle\Entity\InventoryReportItem $inventoryReportItems)
	{
		$this->inventoryReportItems->removeElement($inventoryReportItems);
	}
	
	/**
	 * Get inventoryReportItems
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getInventoryReportItems()
	{
		return $this->inventoryReportItems;
	}
	
        
        /**
	 * Add inventoryReportItems
	 *
	 * @param \CI\InventoryBundle\Entity\InventoryReportItem $inventoryReportItems
	 * @return Product
	 */
	public function addUmReportItem(\CI\InventoryBundle\Entity\UmReportItem $umReportItems)
	{
		$this->inventoryReportItems[] = $umReportItems;
	
		return $this;
	}
        
        /**
	 * Remove inventoryReportItems
	 *
	 * @param \CI\InventoryBundle\Entity\InventoryReportItem $inventoryReportItems
	 */
	public function removeUmReportItem(\CI\InventoryBundle\Entity\UmReportItem $umReportItems)
	{
		$this->inventoryReportItems->removeElement($umReportItems);
	}
	
	/**
	 * Get inventoryReportItems
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getUmReportItems()
	{
		return $this->inventoryReportItems;
	}
        
	/**
	 * Set isCore
	 *
	 * @param boolean $isCore
	 * @return Product
	 */
	public function setIsCore($isCore)
	{
		$this->isCore = $isCore;
	
		return $this;
	}
	
	/**
	 * Get isCore
	 *
	 * @return boolean
	 */
	public function getIsCore()
	{
		return $this->isCore;
	}
	
	/**
	 * Set isNpd
	 *
	 * @param boolean $isNpd
	 * @return Product
	 */
	public function setIsNpd($isNpd)
	{
		$this->isNpd = $isNpd;
	
		return $this;
	}
	
	/**
	 * Get isNpd
	 *
	 * @return boolean
	 */
	public function getIsNpd()
	{
		return $this->isNpd;
	}
	
	/**
	 * Set isOsaTracked
	 *
	 * @param boolean $isOsaTracked
	 * @return Product
	 */
	public function setIsOsaTracked($isOsaTracked)
	{
		$this->isOsaTracked = $isOsaTracked;
	
		return $this;
	}
	
	/**
	 * Get isOsaTracked
	 *
	 * @return boolean
	 */
	public function getIsOsaTracked()
	{
		return $this->isOsaTracked;
	}
	
	/**
	 * Set isSeasonal
	 *
	 * @param boolean $isSeasonal
	 * @return Product
	 */
	public function setIsSeasonal($isSeasonal)
	{
		$this->isSeasonal = $isSeasonal;
	
		return $this;
	}
	
	/**
	 * Get isSeasonal
	 *
	 * @return boolean
	 */
	public function getIsSeasonal()
	{
		return $this->isSeasonal;
	}
	
	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Product
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	
		return $this;
	}
	
	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
	
	public function getLog()
	{
		return array(
			'Name' => $this->getName(),
			'Description' => $this->getDescription(),
			'Core' => $this->getIsCore() ? 'Yes' : 'No',
			'NPD' => $this->getIsNpd() ? 'Yes' : 'No',
			'Seasonal' => $this->getIsSeasonal() ? 'Yes' : 'No',
			'OSA Tracked' => $this->getIsOsaTracked() ? 'Yes' : 'No',
			'Code' => $this->getRefCode(),
			'Size' => $this->getSize(),
			'Category' => $this->getCategory()->getName()
		);
	}
	
	public function isEditable()
	{
// 		$result = false;
		
// 		if ($this->getStatus() < self::STATUS_VOID) {
			$result = true;
// 		}
		
		return $result;
	}
	
	public function isDeletable()
	{
 		if ($this->getInventoryReportItems()->count() > 0) {
			return false;
 		}
		
		return true;
	}
}
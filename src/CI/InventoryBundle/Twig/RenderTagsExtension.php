<?php
namespace CI\InventoryBundle\Twig;

use Twig_Extension;
use Twig_SimpleFunction;
use Twig_SimpleFilter;
use Twig_Filter_Method;
use CI\InventoryBundle\Entity\Device;

class RenderTagsExtension extends Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
    	$options = array('pre_escape' => 'html', 'is_safe' => array('html'));
    	
        return array(
        	new Twig_SimpleFunction('renderTags', array($this, 'renderTagsFunction'), $options),
        	new Twig_SimpleFunction('renderStatusTag', array($this, 'renderStatusTagFunction'), $options),
        	new Twig_SimpleFunction('renderActiveTag', array($this, 'renderActiveTagFunction'), $options),
        	new Twig_SimpleFunction('renderNewTag', array($this, 'renderNewTagFunction'), $options),
        	new Twig_SimpleFunction('renderUpdatedTag', array($this, 'renderUpdatedTagFunction'), $options)
        );
    }
    
    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
    	$options = array('pre_escape' => 'html', 'is_safe' => array('html'));
    	
    	return array(
        	new Twig_SimpleFilter('renderTags', array($this, 'renderTagsFunction'), $options),
        	new Twig_SimpleFilter('renderStatusTag', array($this, 'renderStatusTagFunction'), $options),
        	new Twig_SimpleFilter('renderActiveTag', array($this, 'renderActiveTagFunction'), $options),
        	new Twig_SimpleFilter('renderNewTag', array($this, 'renderNewTagFunction'), $options),
        	new Twig_SimpleFilter('renderUpdatedTag', array($this, 'renderUpdatedTagFunction'), $options)
        );
    }
    
    public function renderTagsFunction($data, $class = null)
    {
    	if (is_object($data)) {
    		$html = '';
    		if (method_exists($data, 'getStatus')) {
    			$result = $this->translateStatus(call_user_func(array($data, 'getStatus')));
    			$html = $this->generateHTML($result["class"], $result["status"], $class);
    		} else if (method_exists($data, 'getActive')) {
    			if (!$data->getActive()) {
    				$html = $this->generateHTML('danger', 'Inactive', $class);
    			}
    		}
    		
    		if (method_exists($data, 'getCreatedAt')) {
    			if ($data->getCreatedAt()->format('m/d/Y') == date('m/d/Y', strtotime('now'))) {
    				$html .=  $this->generateHTML('warning', 'New', $class);
    			}
    		}
    		
    		if (method_exists($data, 'getUpdatedAt') && method_exists($data, 'getCreatedAt')) {
    			if (
    				!is_null($data->getUpdatedAt()) &&
    				$data->getUpdatedAt()->format('m/d/Y') != $data->getCreatedAt()->format('m/d/Y') &&
    				$data->getUpdatedAt()->format('m/d/Y') == date('m/d/Y', strtotime('now'))
    			) {
    				$html .= $this->generateHTML('warning', 'Updated', $class);
    			}
    		}
    		
    		return $html;
    	} else {
    		return $this->generateHTML('default', 'ERROR');
    	}
    }
    
    public function renderStatusTagFunction($data, $class = null)
    {
    	if (is_object($data) && method_exists($data, 'getStatus')) {
    		$result = $this->translateStatus(call_user_func(array($data, 'getStatus')));
    	} else if (!is_object($data)) {
    		$result = $this->translateStatus($data);
    	} else {
    		$result = $this->translateStatus(0);
    	}
    	
        return $this->generateHTML($result["class"], $result["status"], $class);
    }
    
    public function renderActiveTagFunction($data, $class = null)
    {
    	if (is_object($data) && method_exists($data, 'getActive')) {
    		if (!$data->getActive()) {
    			return $this->generateHTML('danger', 'Inactive', $class);
    		}
    	} else {
    		return $this->generateHTML('default', 'ERROR');
    	}
    }
    
    public function renderNewTagFunction($data, $class = null)
    {
    	if (is_object($data) && method_exists($data, 'getCreatedAt')) {
    		if ($data->getCreatedAt()->format('m/d/Y') == date('m/d/Y', strtotime('now'))) {
    			return $this->generateHTML('warning', 'New', $class);
    		}
    	} else {
    		return $this->generateHTML('default', 'ERROR');
    	}
    }
    
    public function renderUpdatedTagFunction($data, $class = null)
    {
    	if (is_object($data) && method_exists($data, 'getUpdatedAt') && method_exists($data, 'getCreatedAt')) {
    		if (
    			!is_null($data->getUpdatedAt()) &&
    			$data->getUpdatedAt()->format('m/d/Y') != $data->getCreatedAt()->format('m/d/Y') &&
    			$data->getUpdatedAt()->format('m/d/Y') == date('m/d/Y', strtotime('now'))
        	) {
    			return $this->generateHTML('warning', 'Updated', $class);
    		}
    	} else {
    		return $this->generateHTML('default', 'ERROR');
    	}
    }
    
    public function generateHTML($labelClass, $labelContent, $class = null)
    {
    	return sprintf(' <span class="label label-%s%s">%s</span>', $labelClass, is_null($class) ? "" : " " . $class , $labelContent);
    }
    
    public function translateStatus($statusData)
    {
    	$info = array(
    		10 => 'Draft',
    		11 => 'Ongoing',
    		15 => 'Pending',
    		70 => 'For Approval',
    		'Draft' => 'Draft',
    		'Pending for Approval' => 'Pending for Approval'
    	);
    	
    	$success = array(
    		20 => 'Approved',
    		21 => 'Completed',
    		23 => 'Returned',
    		25 => 'Delivered',
    		90 => 'Approved',
    		'Approved' => 'Approved',
    		'Received' => 'Received',
    		'Working' => 'Working'
    	);
    	
    	$warning = array(
    		80 => 'For Revision',
    		'Repair' => 'Repair'
    	);
    	
    	$danger = array(
    		30 => 'Void',
    		'Void' => 'Void',
    		'Broken' => 'Broken'
    	);
    	
    	$default = array(
    		40 => 'Closed'
    	);
    	
    	if (isset($info[$statusData])) {
    		$status = array(
	    		'class' => 'info',
	    		'status' => $info[$statusData]
	    	);
    	} else if (isset($success[$statusData])) {
    		$status = array(
	    		'class' => 'success',
	    		'status' => $success[$statusData]
	    	);
    	} else if (isset($warning[$statusData])) {
    		$status = array(
	    		'class' => 'warning',
	    		'status' => $warning[$statusData]
	    	);
    	} else if (isset($danger[$statusData])) {
    		$status = array(
	    		'class' => 'danger',
	    		'status' => $danger[$statusData]
	    	);
    	} else if (isset($default[$statusData])) {
    		$status = array(
	    		'class' => 'default',
	    		'status' => $default[$statusData]
	    	);
    	} else {
    		$status = array(
    			'class' => 'default',
    			'status' => 'Error'
    		);
    	}
    	
    	return $status;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'ci_render_tags';
    }
}
<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', 'text', array(
			'label' => 'Name',
			'required' => false
		))
		->add('refCode', 'text', array(
			'label' => 'Code',
			'required' => false
		))
		->add('category', 'entity', array(
			'required' => false,
			'class' => 'CIInventoryBundle:Category',
			'empty_value' => 'Choose a category',
			'property' => 'name',
			'attr' => array('class' => 'category select2'),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('status', 'choice', array(
			'required' => false,
			'empty_value' => 'All',
			'data' => true,
			'choices' => array(
				false => 'Inactive',
				true => 'Active'
			)
		))
		->add('isCore', 'choice', array(
			'label' => 'Core',
			'required' => false,
			'empty_value' => 'All',
			'choices' => array(
				false => 'No',
				true => 'Yes'
			)
		))
		->add('isNpd', 'choice', array(
			'label' => 'NPD',
			'required' => false,
			'empty_value' => 'All',
			'choices' => array(
				false => 'No',
				true => 'Yes'
			)
		))
		->add('isOsaTracked', 'choice', array(
			'label' => 'Promo Packs',
			'required' => false,
			'empty_value' => 'All',
			'choices' => array(
				false => 'No',
				true => 'Yes'
			)
		))
		->add('isSeasonal', 'choice', array(
			'label' => 'Seasonal',
			'required' => false,
			'empty_value' => 'All',
			'choices' => array(
				false => 'No',
				true => 'Yes'
			)
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_productfilter';
	}
}
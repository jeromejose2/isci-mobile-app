<?php

namespace CI\InventoryBundle\Form\Type;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CI\InventoryBundle\Form\DataTransformer\EntityToIdTransformer;
 
class EntityAutocompleteType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;
 
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }
 
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$transformer = new EntityToIdTransformer($this->objectManager, $options['class'], true);
        $builder->addModelTransformer($transformer);
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    	$resolver->setDefaults(array(
    		'class' => null,
    		'invalid_message' => 'The selected entity does not exist',
    	));
    }
 
    public function getParent()
    {
        return 'text';
    }
 
    public function getName()
    {
        return 'entity_autocomplete';
    }
}
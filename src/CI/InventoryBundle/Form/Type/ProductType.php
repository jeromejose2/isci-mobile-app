<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', 'text', array(
			'label' => 'Name'
		))
		->add('description')
		->add('isCore', 'checkbox', array(
			'required' => false,
			'label' => 'Core',
			'attr'=> array(
				'class' => 'px',
				'align_with_widget' => true
			)
		))
		->add('isNpd', 'checkbox', array(
			'required' => false,
			'label' => 'NPD',
			'attr'=> array(
				'class' => 'px',
				'align_with_widget' => true
			)
		))
		->add('isSeasonal', 'checkbox', array(
			'required' => false,
			'label' => 'Seasonal',
			'attr'=> array(
				'class' => 'px',
				'align_with_widget' => true
			)
		))
		->add('isOsaTracked', 'checkbox', array(
			'required' => false,
			'label' => 'OSA Tracked',
			'attr'=> array(
				'class' => 'px',
				'align_with_widget' => true
			)
		))
		->add('refCode', 'text', array(
			'label' => 'Code'
		))
		->add('size', 'text', array(
			'label' => 'Size'
		))
		->add('category', 'entity', array(
			'class' => 'CIInventoryBundle:Category',
			'empty_value' => 'Choose a category',
			'property' => 'name',
			'attr' => array('class' => 'category select2'),
			'query_builder' => function($er) {
				return $er->findAllActiveQb();
			}
		))
		->add('stores', 'entity', array(
			'class' => 'CIInventoryBundle:Store',
			'label' => 'Branches',
			'empty_value' => 'Choose branches',
			'property' => 'name',
			'multiple' => true,
			'attr' => array('class' => 'stores', 'size' => '11'),
			'query_builder' => function($er) {
				return $er->findAllQb();
			}
		))
		->add('save', 'submit', array(
			'label' => 'Save',
			'attr' => array(
				'class' => 'btn btn-primary submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Product'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_product';
	}
}
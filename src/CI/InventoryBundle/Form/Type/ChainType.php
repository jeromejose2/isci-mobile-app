<?php

namespace CI\InventoryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChainType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('isParent', 'checkbox', array(
			'required' => false,
			'label' => 'Parent Chain',
			'attr'=> array(
				'class' => 'is-parent px',
				'align_with_widget' => true
			)
		))
		->add('name')
		->add('isKeyAccount', 'checkbox', array(
			'required' => false,
			'label' => 'Key Account',
			'attr'=> array('align_with_widget' => true)
		))
		->add('isOsaTracked', 'checkbox', array(
			'required' => false,
			'label' => 'OSA Tracked',
			'attr'=> array('align_with_widget' => true)
		))
		->add('save', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-success submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
		
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
			$form = $event->getForm();
			$data = $event->getData();
			$required = false;
				
			if (!$data->getIsParent()) {
				$required = true;
			}
				
			$formOptions = array(
				'class' => 'CIInventoryBundle:Chain',
				'required' => $required,
				'empty_value' => 'Choose a parent',
				'property' => 'name',
				'label' => 'Parent',
				'attr' => array('class' => 'select2'),
				'query_builder' => function($repository) use ($data) {
					$qb = $repository->findAllQb(true);
						
					if ($data->getId()) {
						$qb->andWhere('c.id != :id')
						->setParameter('id', $data->getId());
					}
						
					return $qb;
				}
			);
				
			$form->add('parent', 'entity', $formOptions);
		});
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\InventoryBundle\Entity\Chain'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_inventorybundle_chain';
	}
}
<?php

namespace CI\InventoryBundle\Form\DataTransformer;
 
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
 
class CustomNumberTransformer implements DataTransformerInterface
{
    public function transform($number)
    {
        return $number;
    }
 
    public function reverseTransform($input)
    {
    	if (strpos($input, ',') !== false)
    		return str_replace(',', '', $input);
    	else 
    		return $input;
    }
}

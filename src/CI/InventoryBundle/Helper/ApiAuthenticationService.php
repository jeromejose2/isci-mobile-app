<?php

namespace CI\InventoryBundle\Helper;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class ApiAuthenticationService
{
	protected $em;
	
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}
	
	/**
	 * Checks whether a request has the right credentials
	 * 
	 * Returns the device ID if successful
	 * Returns an array containing an error if unsuccessful
	 * 
	 * @param Request $request
	 * @return array
	 */
	public function isAuthenticated(Request $request)
	{
		$em = $this->em;
		
		//get appID, appSecret & deviceID headers
		$appId = $request->headers->get('appID');
		$appSecret = $request->headers->get('appSecret');
		$deviceId = $request->headers->get('deviceID');
		
		//check if appID, appSecret & deviceID are empty
		if (empty($appId) || empty($appSecret) || empty($deviceId)) {
			return $this->getErrorResponse();
		}
		
		//check if the deviceID exists
		$device = $em->getRepository('CIInventoryBundle:Device')->findOneBy(array('deviceId' => $deviceId, 'active' => 1));
		if (empty($device)) {
			return $this->getErrorResponse();
		}
		
		//check if appID & appSecret = system's appID & appSecret
		$mobileAppSettings = $em->getRepository('CIInventoryBundle:MobileApp')->find(1);
		if ($mobileAppSettings->getAppId() != $appId || $mobileAppSettings->getAppSecret() != $appSecret) {
			return $this->getErrorResponse();
		}
		
		//return the device identifier
		return $device;
	}
	
	/**
	 * Returns array with error message
	 * 
	 * @return array
	 */
	public function getErrorResponse()
	{
		return false;
	}
}
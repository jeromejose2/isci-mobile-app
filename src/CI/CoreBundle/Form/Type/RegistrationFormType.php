<?php

namespace CI\CoreBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

use CI\CoreBundle\Entity\User;

class RegistrationFormType extends BaseType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);
		
		$builder
		->add('firstName', 'text', array(
			'label' => 'First Name'
		))
		->add('lastName', 'text', array(
			'label' => 'Last Name',
			'required' => false
		))
		->add('username')
		->add('email', 'email')
		->add('plainPassword', 'repeated', array(
			'required' => true,
			'first_options'  => array('label' => 'Password'),
			'second_options' => array('label' => 'Repeat Password'),
			'type' => 'password'
		))
		->add('gender', 'choice', array(
			'expanded' => true,
			'multiple' => false,
			'required' => true,
			'attr' => array('inline' => true),
			'choices' => array(
				'M' => 'Male',
				'F' => 'Female'
			)
		))
		->add('birthDate', 'date', array(
			'label'    => 'Birthday',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'required' => false,
			'attr'	   => array(
				'datepicker' => true,
				'input_group' => array('append' => 'calendar'),
			)
		))
		->add('formRole', 'choice', array(
			'required' => true,
			'label' => 'Role',
			'empty_value' => 'Choose a role',
			'attr' => array('class' => 'role'),
			'constraints' => array(new NotBlank(array('message' => 'Please choose a role for this user'))),
			'choices' => array(
				User::ROLE_ADMIN => 'Admin',
				User::ROLE_IT_ACCOUNT => 'IT Account',
				User::ROLE_RSM => 'RSM',
				User::ROLE_CDM => 'CDM',
				User::ROLE_TL => 'TL',
				User::ROLE_DISER => 'DISER'
			)
		))
		->add('contactNumber', 'text', array(
			'label' => 'Contact Number',
			'required' => false
		))
		;
	}
	
	public function getName()
    {
    	return 'ci_core_user_registration';
    }
}
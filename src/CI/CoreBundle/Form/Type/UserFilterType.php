<?php

namespace CI\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CI\CoreBundle\Entity\User;

class UserFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', 'text', array(
			'required' => false
		))
		->add('role', 'choice', array(
			'required' => false,
			'label' => 'Role',
			'attr' => array('widget_col' => 4),
			'empty_value' => 'All roles',
			'choices' => array(
				User::ROLE_ADMIN => 'Admin',
				User::ROLE_IT_ACCOUNT => 'IT Account',
				User::ROLE_RSM => 'RSM',
				User::ROLE_CDM => 'CDM',
				User::ROLE_TL => 'TL',
				User::ROLE_DISER => 'DISER'
			)
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_corebundle_userfilter';
	}
}
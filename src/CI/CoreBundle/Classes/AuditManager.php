<?php

namespace CI\CoreBundle\Classes;

use Doctrine\ORM\EntityManager;
use SimpleThings\EntityAudit\AuditManager as BaseAuditManager;
use CI\CoreBundle\Classes\AuditReader;

/**
 * Audit Manager extends SimpleThings\EntityAudit\AuditManager;
 */
class AuditManager extends BaseAuditManager
{
    public function createAuditReader(EntityManager $em)
    {
        return new AuditReader($em, $this->getConfiguration(), $this->getMetadataFactory());
    }
}
<?php
namespace CI\CoreBundle\Twig;

class TwigUtilityExtension extends \Twig_Extension
{
	private $router;
	
	public function __construct(\Symfony\Bundle\FrameworkBundle\Routing\Router $router)
	{
		$this->router = $router;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getFunctions()
	{
		return array(
			new \Twig_SimpleFunction('pathExists', array($this, 'pathExists'))
		);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('pathExists', array($this, 'pathExists'))
		);
	}
	
	public function pathExists($path)
	{
		return (null === $this->router->getRouteCollection()->get($path)) ? false : true;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getName()
	{
		return 'ci_twig_utility';
	}
}
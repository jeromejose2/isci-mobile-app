var BRCtrl = {
	branchRequest: null,
	productRequest: null,
	
	init: function() {
		var that = this;
		$( "select.region, select.chain" ).on( "change", function( e ) {
			that.refreshBranch( $( "select.region" ).select2( "val" ), $( "select.chain" ).select2( "val" ) );
		});
		
		$( "select.branch" ).on( "change", function( e ) {
			that.refreshProduct( $( this ).select2( "val" ) );
		});
	},
	
	refreshBranch: function( $region, $chain ) {
		var that = this;
		if ( that.branchRequest ) {
			that.branchRequest.abort();
			that.branchRequest = null;
		}
		 
		if ( $region && $chain ) {
			that.branchRequest = $.getJSON( app.base_uri + '/admin/store/' + $region + '/' + $chain + '/get', function( data ) {
				if ( data !== undefined && !$.isEmptyObject( data ) ) {
					var selected = $( "select.branch" ).select2( "val" );
					var found = false;
					$( "select.branch" ).html( "" );
					$( "select.branch" ).append( $( "<option/>" ).text( "Choose a branch" ).prop( "disabled", true ).val( "" ) );
					
					$.each( data, function( key, value ) {
						$( "select.branch" ).append( $( "<option/>" ).val( value.id ).text( value.name ) );
						if ( value.id == selected ) {
							found = true;
						}
					});
					
					$( "select.branch" ).select2( "val", found ? selected : "" );
				} else {
					$( "select.branch" ).html( "" );
					$( "select.branch" ).append( $( "<option/>" ).text( "Choose a branch" ).prop( "disabled", true ).val( "" ) );
					$( "select.branch" ).select2( "val", "" );
				}
			}).done( function() {
				that.branchRequest = null;
			});
		}
	},
	
	refreshProduct: function( $branch ) {
		var that = this;
		if ( that.productRequest ) {
			that.productRequest.abort();
			that.productRequest = null;
		}
		
		if ( $branch ) {
			that.branchRequest = $.getJSON( app.base_uri + '/admin/product/' + $branch + '/get', function( data ) {
				if ( data !== undefined && !$.isEmptyObject( data ) ) {
					var selected = $( "select.product" ).select2( "val" );
					$( "select.product" ).html( "" );
					$( "select.product" ).append( $( "<option/>" ).text( "All products" ).prop( "disabled", true ).val( "" ) );
					
					$.each( data, function( key, value ) {
						$( "select.product" ).append( $( "<option/>" ).val( value.id ).text( value.name ) );
					});
					
					$( "select.product" ).select2( "val", selected );
				}
			}).done( function() {
				that.productRequest = null;
			});
		}
	}
}

$(function() {
	BRCtrl.init();
});